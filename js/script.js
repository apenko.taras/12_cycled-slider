const imgPath = 'style/img/';
const images = ['1.jpg', '2.jpg', '3.JPG', '4.png'];

let sec = 0;
let started = false

function changeImage() {
    let img;
    sec++;
    started = true

    if (sec === 1) {
        img = images[0];
    } else if (sec === 2) {
        img = images[1];
    } else if (sec === 3) {
        img = images[2];
    } else if (sec === 4) {
        img = images[3];
    }

    if (img) {
        document.getElementById('image-to-show').src = imgPath + img;
    }
    if (sec === 4){
        sec = 0
    }
}

let clock = setInterval(changeImage, 3000);

document.getElementById('restart').addEventListener('click', function() {
    if (!started) {
        clock = setInterval(changeImage, 3000);
    }
})

document.getElementById('stop').addEventListener('click', function() {
    clearInterval(clock)
    started = false
})